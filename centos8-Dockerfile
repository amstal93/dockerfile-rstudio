FROM centos:8

ARG RSTUDIO_VERSION
ENV RSTUDIO_VERSION=${RSTUDIO_VERSION:-1.2.5033}
ARG R_VERSION
ENV R_VERSION=${R_VERSION:-3.5.2}
ARG S6_VERSION
ENV S6_VERSION=${S6_VERSION:-v1.22.1.0}
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2
ENV PATH=/opt/R/${R_VERSION}/bin:$PATH

RUN yum update -y \
  && yum install -y \
  dnf-plugins-core \
  && yum config-manager -y --set-enabled PowerTools \
  && yum install -y \
  bzip2-devel \
  cairo-devel \
  curl \
  fontconfig-devel \
  gcc \
  gcc-c++ \
  gcc-gfortran \
  git \
  glibc-langpack-en \
  initscripts \
  java-1.8.0-openjdk \
  java-1.8.0-openjdk-devel \
  libcurl-devel \
  libgit2-devel \
  libpng-devel \
  libX11-devel \
  libXt-devel \
  libxml2-devel \
  make \
#  mariadb-connector-c-devel \
#  mariadb-devel \
  mysql-devel \
  pango-devel \
  pango \
  perl \
  pcre-devel \
  readline-devel \
  sudo \
  which \
  wget \
  xorg-x11-server-devel \
  xterm \
  xz-devel \
  zlib-devel \
  && yum clean all

## Install libssh2
RUN wget https://www.libssh2.org/download/libssh2-1.9.0.tar.gz \
  && tar zxvf libssh2-1.9.0.tar.gz \
  && cd libssh2-1.9.0 \
  && ./configure \
  && make \
  && make install \
  && cd .. \
  && rm -rf *libssh2*

## Install R
RUN mkdir -p /sources/R \
  && cd /sources/R \
  && wget https://cran.r-project.org/src/base/R-3/R-${R_VERSION}.tar.gz \
  && tar xzf R-${R_VERSION}.tar.gz \
  && cd R-${R_VERSION} \
  && ./configure --prefix=/opt/R/${R_VERSION} --enable-R-shlib \
  && make \
  && make install \
  && cd / \
  && rm -r /sources/R \
  ## Add a default CRAN mirror
  && echo "options(repos = c(CRAN='https://cloud.r-project.org'), download.file.method = 'libcurl')" >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  ## Use littler installation scripts
  && /opt/R/${R_VERSION}/bin/R -e "install.packages(c('littler', 'docopt'), repo = 'https://cloud.r-project.org')" \
  ## Set up links
  && ln -s /opt/R/${R_VERSION}/lib64/R/library/littler/examples/install2.r /usr/bin/install2.r \
  && ln -s /opt/R/${R_VERSION}/lib64/R/library/littler/examples/installGithub.r /usr/bin/installGithub.r \
  && ln -s /opt/R/${R_VERSION}/lib64/R/library/littler/bin/r /usr/bin/r \
  ## Clean up from R source install
  && cd / \
  && rm -rf /tmp/* \
  && rm -rf /var/cache/yum/

## Install Rstudio
RUN RSTUDIO_LATEST=$(wget --no-check-certificate -qO- https://s3.amazonaws.com/rstudio-server/current.ver) \
  && [ -z "$RSTUDIO_VERSION" ] && RSTUDIO_VERSION=$RSTUDIO_LATEST || true \
  && wget -q https://download2.rstudio.org/server/fedora28/x86_64/rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm \
  && gpg --keyserver keys.gnupg.net --recv-keys 3F32EE77E331692F \
  && gpg --export --armor 3F32EE77E331692F > rstudio-signing.key \
  && rpm --import rstudio-signing.key \
  && rpm -K rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm \
  && yum install -y --nogpgcheck rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm \
  && rm rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm

RUN mkdir -p /etc/R \
  ## Write config files in $R_HOME/etc
  && echo '' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo '# Configure httr to perform out-of-band authentication if HTTR_LOCALHOST' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo '# is not set since a redirect to localhost may not work depending upon' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo '# where this Docker container is running.' >>  /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo 'if(is.na(Sys.getenv("HTTR_LOCALHOST", unset=NA))) {' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo '  options(httr_oob_default = TRUE)' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo '}' >> /opt/R/${R_VERSION}/lib64/R/etc/Rprofile.site \
  && echo "PATH=${PATH}" >> /opt/R/${R_VERSION}/lib64/R/etc/Renviron \
  ## Need to configure non-root user for RStudio
  && useradd rstudio \
  && echo "rstudio:rstudio" | chpasswd \
  && mkdir -p /home/rstudio \
  && chown rstudio:rstudio /home/rstudio \
  ## Prevent rstudio from deciding to use /usr/bin/R if a user yum installs a package
  && echo "rsession-which-r=/opt/R/${R_VERSION}/bin/R" >> /etc/rstudio/rserver.conf \
  ## use more robust file locking to avoid errors when using shared volumes:
  && echo 'lock-type=advisory' >> /etc/rstudio/file-locks \
  ## configure git not to request password each time
  && git config --system credential.helper 'cache --timeout=3600' \
  && git config --system push.default simple

## Set up S6 init system
RUN wget -P /tmp/ https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-amd64.tar.gz \
  # Fix for s6-overlay in centos: https://github.com/just-containers/s6-overlay/issues/125#issuecomment-321615127
  && tar xzf /tmp/s6-overlay-amd64.tar.gz -C / --exclude="./bin" \
  && tar xzf /tmp/s6-overlay-amd64.tar.gz -C /usr ./bin \
  && mkdir -p /etc/services.d/rstudio \
  && echo '#!/usr/bin/with-contenv bash' > /etc/services.d/rstudio/run \
  && echo '## load /etc/environment vars first:' >> /etc/services.d/rstudio/run \
  && echo 'for line in $( cat /etc/environment ) ; do export $line ; done' >> /etc/services.d/rstudio/run \ 
  && echo '  exec /usr/lib/rstudio-server/bin/rserver --server-daemonize 0' >> /etc/services.d/rstudio/run \ 
  && echo '#!/bin/bash' > /etc/services.d/rstudio/finish \
  && echo 'rstudio-server stop' >> /etc/services.d/rstudio/finish \
  && mkdir -p /home/rstudio/.rstudio/monitored/user-settings \
  && echo 'alwaysSaveHistory="0"' > /home/rstudio/.rstudio/monitored/user-settings/user-settings \
  && echo 'loadRData="0"' >> /home/rstudio/.rstudio/monitored/user-settings/user-settings \
  && echo 'saveAction="0"' >> /home/rstudio/.rstudio/monitored/user-settings/user-settings \
  && chown -R rstudio:rstudio /home/rstudio/.rstudio \
  && rm -rf /tmp/*

RUN /opt/R/${R_VERSION}/bin/R -e "install.packages(c('cowplot','devtools','digest','DBI','dplyr','DT','flextable','gdata','gdtools','ggforce','ggplot2','htmltools','htmlwidgets','jsonlite','knitr','magrittr','NonCompart','officer','openxlsx','PKNCA','plyr','pspline','RCurl','rio','rlang','RMySQL','scales','shiny','shinydashboard','shinyjs','shinyWidgets','stringi','stringr','systemfonts','testthat','tidyr','vpc','xlsx','xtable','yaml'), repos = 'https://cloud.r-project.org')"

## running with "-e ADD=shiny" adds shiny server
COPY add_shiny.sh /etc/cont-init.d/add
COPY disable_auth_rserver.conf /etc/rstudio/disable_auth_rserver.conf
COPY pam-helper.sh /usr/lib/rstudio-server/bin/pam-helper
COPY userconf.sh /etc/cont-init.d/userconf

## Automatically link a shared volume
VOLUME /home/rstudio/data

ENV PASSWORD=password

EXPOSE 8787

CMD ["/init"]
