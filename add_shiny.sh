#!/usr/bin/with-contenv bash

ADD=${ADD:=none}

## A script to add shiny to an rstudio-based rocker image.  

[ "$ADD" == "shiny" ] \
  && echo "Adding shiny server to container..."	\
  && wget --no-verbose https://s3.amazonaws.com/rstudio-shiny-server-os-build/centos6.3/x86_64/VERSION -O version.txt \
  && VERSION=$(cat version.txt) \
  && wget --no-verbose https://s3.amazonaws.com/rstudio-shiny-server-os-build/centos6.3/x86_64/shiny-server-$VERSION-x86_64.rpm -O ss-latest.rpm \
  && yum install -y ss-latest.rpm \
  && rm -f version.txt ss-latest.rpm \
  && install2.r -e --skipinstalled shiny rmarkdown \
  && cp -R /opt/R/3.5.2/lib64/R/library/shiny/examples/* /srv/shiny-server/ \
  && rm -rf /var/cache/yum/ \
  && mkdir -p /var/log/shiny-server \
  && chown shiny.shiny /var/log/shiny-server \
  && mkdir -p /etc/services.d/shiny-server \
  && cd /etc/services.d/shiny-server \
  && echo '#!/bin/bash' > run \
  && echo 'exec shiny-server > /var/log/shiny-server.log' >> run \
  && chmod +x run \
  && useradd -G rstudio shiny \
  && cd /

[ $"$ADD" == "none" ] && echo "Nothing additional to add"
